import React from 'react';
import Highcharts from 'highcharts';
import HighchartsReact from 'highcharts-react-official';
import Data from '../data/data.json';

const Categories = Object.keys(Data);

class Dashboard extends React.Component {
  constructor(props) {
    super(props);
    this.generateSeriesData = this.generateSeriesData.bind(this);
    this.updateSeriesData = this.updateSeriesData.bind(this);
    this.handleClick = this.handleClick.bind(this);

    this.state = {
      dataSet: [],
      seriesData: []
    }
  }

  componentWillMount() {
    this.updateSeriesData();
  }

  generateSeriesData(seriesArr, dataValue) {
    this.state.dataSet.forEach(function(key) {
      if(!seriesArr.some( val => val['name'] === key )) {
        seriesArr.push(
          {
            name: key,
            data: [dataValue[key]]
          }
        )
      }
      else {
        for (let i = 0; i < seriesArr.length; i++) {
          if (seriesArr[i].name === key) {
            seriesArr[i].data.push(dataValue[key]);
            switch (seriesArr[i].name) {
              case 'Critical':
                seriesArr[i].color = '#e55242';
                break;
              case 'High':
                seriesArr[i].color = '#e88542';
                break;
              case 'Medium':
                seriesArr[i].color = '#8bdef2';
                break;
              case 'Low':
                seriesArr[i].color = '#f7f79f';
                break;
              default: // do nothing
            }
            break;
          }
        }
      }
    });
    this.setState({
      seriesData: seriesArr
    });
  }

  updateSeriesData() {
    const seriesArr = [];
    Categories.forEach((value) => {
      const dataValue = Data[value];
      const newKeys = Object.keys(dataValue);
      if (!this.state.dataSet || !this.state.dataSet.length) {
        this.setState({
          dataSet: newKeys
        }, () => {
          this.generateSeriesData(seriesArr, dataValue);
        });
      } else {
        this.generateSeriesData(seriesArr, dataValue);
      }
    })
  }

  handleClick(value) {
    this.setState({
      dataSet: value
    }, () => {
      this.updateSeriesData();
    });
  }

  render() {

    const chartOptions = {
      title: {
        text: 'Priorities Chart'
      },
      chart: {
        type: 'column',
        height: 500
      },
      xAxis: {
        categories: Categories,
      },
      yAxis: {
        title: {
          text: 'Values (%)'
        }
      },
      series: this.state.seriesData,
      colors: [
        '#e55242',
        '#e88542',
        '#8bdef2',
        '#f7f79f'
      ],
      tooltip: {
        headerFormat: '',
        pointFormat: '<span class="name">#{series.name}</span>: {point.y} ({point.percentage:.0f}%)',
        style: {
          color: '#ffffff',
          fontSize: '10px'
        },
        backgroundColor: '#666666',
        borderColor: '#666666',
        borderWidth: 0,
        shadow: false
      },
      plotOptions: {
        column: {
          stacking: 'percent',
          events: {
            legendItemClick: () => false
          }
        },
        series: {
          pointWidth: 100
        }
      }
    };

    return (
      <div>
        <HighchartsReact
          highcharts={Highcharts}
          options={chartOptions}
        />
        <div className="btn-group">
          <p>Click option to filter risk level</p>
          <button onClick={() => this.handleClick(['Critical', 'High', 'Medium', 'Low'])}>Original</button>
          <button onClick={() => this.handleClick(['Critical'])}>Critical only</button>
          <button onClick={() => this.handleClick(['Critical', 'High'])}>Critical/High</button>
          <button onClick={() => this.handleClick(['Critical', 'High', 'Medium'])}>Critical/High/Medium</button>
          <button onClick={() => this.handleClick(['Low'])}>Low only</button>
        </div>
      </div>
    );
  }
}

export default Dashboard;
