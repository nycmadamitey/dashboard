import React from 'react';
import { injectGlobal } from 'emotion';
import Dashboard from './components/Dashboard';
import styles from './styles';

injectGlobal`${styles}`

function App() {
  return (
    <>
      <Dashboard />
    </>
  );
}

export default App;
