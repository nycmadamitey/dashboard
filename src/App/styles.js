export default `
  * {
    box-sizing: border-box;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
  }

  html, body {
    font-size: 16px;
    font-family: Helvetica;
    color: #000;
    position: relative;
    margin: 0;
    padding: 0;
    height: 100%;
    width: 100%;
    overflow-x: hidden;
  }

  #root {
    max-width: 900px;
    margin: 0 auto;
  }

  .highcharts-tooltip {
    .name {
      text-transform: uppercase;
    }
  }

  .btn-group {
    display: flex;
    flex-wrap: wrap;
    justify-content: space-evenly;
    max-width: 500px;
    margin: 0 auto;

    p {
      flex-basis: 100%;
      text-align: center;
      margin-top: 0;
    }

    button {
      -webkit-appearance: none;
      font-size: 12px;
      border: 1px solid #ccc;
      padding: 5px 10px;
      border-radius: 3px;
      cursor: pointer;
    }
  }
`
